class RouteSerializer < ActiveModel::Serializer
  attributes :id
  attributes :title, :description, :grade
  attributes :created_at, :updated_at

  has_many :images
end

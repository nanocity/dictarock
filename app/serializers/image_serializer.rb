class ImageSerializer < ActiveModel::Serializer
  attributes :id
  attributes :url
  attributes :created_at, :updated_at

  def url
    object.attachment.url
  end
end

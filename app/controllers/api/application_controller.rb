module Api
  class ApplicationController < ActionController::Base
    include DeviseTokenAuth::Concerns::SetUserByToken

    respond_to :json

    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    # Not needed since devise_auth_token use rotative tokens
    #protect_from_forgery with: :null_session

    rescue_from ActiveRecord::RecordNotFound do
      respond_with nil, status: 404
    end
  end
end

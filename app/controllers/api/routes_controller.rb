module Api
  class RoutesController < Api::ApplicationController
    before_action :authenticate_user!, only: [:create, :update, :destroy]

    respond_to :json

    def index
      @routes = Route.all
      respond_with @routes
    end

    def show
      @route = Route.find(params[:id])
      respond_with @route
    end

    def create
      @route = Route.create(create_params)
      respond_with @route, location: api_route_path(@route)
    end

    def update
      @route = Route.find(params[:id])
      @route.update_attributes(update_params)
      respond_with @route, location: api_route_path(@route)
    end

    def destroy
      @route = Route.find(params[:id])
      @route.destroy
      respond_with @route, location: api_route_path(@route)
    end

    private

    def create_params
      params.require(:route).permit(:title, :description, :grade, images_attributes: [:attachment])
    end

    def update_params
      params.require(:route).permit(
        :title, :description, :grade, images_attributes: [
          :id, :attachment, :_destroy
        ]
      )
    end
  end
end

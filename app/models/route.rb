class Route < ActiveRecord::Base
  validates :title, presence: true
  validates :grade, format: { with: /\A((III|IV|V)\+?)|(?:[6-9][a-c]\+?)\z/ }, allow_blank: true

  has_many :images, dependent: :destroy

  accepts_nested_attributes_for :images, allow_destroy: true
end

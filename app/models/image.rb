class Image < ActiveRecord::Base
  validates :attachment, presence: true

  belongs_to :route

  mount_uploader :attachment, AttachmentUploader
end

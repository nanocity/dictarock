class AddGradeToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :grade, :string, default: "unknown"
  end
end

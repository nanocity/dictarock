class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :attachment
      t.timestamps
      t.references :route, index: true, foreign_key: true
    end
  end
end

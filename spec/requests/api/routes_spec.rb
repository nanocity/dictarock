RSpec.describe 'API Routes' do
  describe '#GET /api/routes' do
    context 'when route exists' do
      it 'return the specified route by its ID' do
        route = FactoryGirl.create(:route, :with_images)

        headers = { 'ACCEPT' => 'application/json' }
        get "/api/routes/#{route.id}", nil, headers

        expect(response.content_type).to eq('application/json')
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when route does not exist' do
      it 'return a not found response' do
        headers = { 'ACCEPT' => 'application/json' }
        get '/api/routes/1', nil, headers

        expect(response.content_type).to eq('application/json')
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end

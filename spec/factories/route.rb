FactoryGirl.define do
  factory :route do
    title { Faker::StarWars.planet }
    description { Faker::Lorem.sentence }
    grade 'V+'

    trait :with_images do
      after(:create) do |instance|
        create_list :image, 2, route: instance
      end
    end
  end
end

FactoryGirl.define do
  factory :image do
    attachment do
      Rack::Test::UploadedFile.new(
        File.join(Rails.root, 'spec', 'support', 'images', 'route.png')
      )
    end

    association :route
  end
end

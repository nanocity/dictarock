module ControllerMacros
  def content_type(format)
    before(:each) do
      @request.env['CONTENT_TYPE'] = format
      @request.env['HTTP_ACCEPT'] = format
      @request.env['ACCEPT'] = format
    end
  end
end

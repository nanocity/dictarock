require 'rails_helper'

RSpec.describe Image do
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:attachment) }
  end

  describe 'Relations' do
    it { is_expected.to belong_to(:route) }
  end
end

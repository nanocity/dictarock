require 'rails_helper'

RSpec.describe Route do
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:title) }


    it 'validates standar grades' do
      route = FactoryGirl.create(:route)

      %w{III IV IV+ V V+ 6a 6b 6c 6a+ 9c+}.each do |grade|
        route.grade = grade
        expect(route).to be_valid
      end
    end
  end

  describe 'Relations' do
    it { is_expected.to have_many(:images) }
  end
end

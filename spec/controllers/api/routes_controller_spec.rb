RSpec.describe Api::RoutesController do
  content_type 'application/json'

  describe 'GET #show' do
    let(:route) { FactoryGirl.create(:route, :with_images) }
    context 'when resource exists' do
      it 'returns the route specified by it ID' do
        get :show, id: route

        expect(parsed_body['id']).to eq(route.id)
      end
    end
  end
end

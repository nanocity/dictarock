source 'https://rubygems.org'

ruby '2.2.3'

# Template gems
gem 'rails', '4.2.6'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'

gem 'haml-rails'
gem 'pg'

# ENV variables
gem 'dotenv-rails'

# CORS
gem 'rack-cors', require: 'rack/cors'

# Upload attachments such as route images
gem 'carrierwave'

# Use JSON for API
gem 'responders'
gem 'active_model_serializers', github: 'rails-api/active_model_serializers'

# Authentication
gem 'omniauth'
gem 'omniauth-github'
gem 'devise_token_auth'

group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring'
  gem 'better_errors'
  gem 'foreman'
  gem 'html2haml'
  gem 'rails_layout'
  gem 'spring-commands-rspec'
end

group :test do
  gem 'shoulda-matchers'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'rspec-rails'
  gem 'rubocop'
  gem 'byebug'
end

group :production do
  gem 'puma'
  gem 'rails_12factor'
end
